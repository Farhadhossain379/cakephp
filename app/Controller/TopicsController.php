<?php

class TopicsController extends AppController {

    public $components = array('Session');

    public function add(){
        if($this->request->is('post')){
            $this->Topic->create();
            if($this->Topic->save($this->request->data)){
                $this->Session->setFlash('Topic has been created');
                $this->redirect('index');
            }
        }
    }

    public function index(){
        $topics = $this->Topic->find('all');
        if($topics){
            $this->set('topics',$topics);
        }
    }

    public function view($id){
        $data = $this->Topic->findById($id);
        $this->set('topic',$data);
    }
    public function edit($id){
        $data = $this->Topic->findById($id);
        if($this->request->is (array('post','put'))){
            $this->Topic->id=$id;
            if($this->Topic->save($this->request->data)){
                $this->Session->setFlash('Topic has been edited');
                $this->redirect('index');
            }
        }
        $this->request->data = $data;
    }

    public function delete($id){
        $data = $this->Topic->findById($id);
        if($this->request->is (array('post','put'))){
            $this->Topic->id=$id;
            if($this->Topic->delete()){
                $this->Session->setFlash('Topic has been deleted');
                $this->redirect('index');
            }
        }
    }
}
