<?php
class BooksController extends AppController {

//    from browser: http://localhost:8080/cakephp/books
//    from browser: http://localhost:8080/cakephp/books/index/1
//    from browser: http://localhost:8080/cakephp/books/index/2

    var $name = 'Books';
    var $uses = array();
    public $components = array('Util');

    function index($id=0) {
        $books = array (
            '0' => array (
                'bookTitle' => 'Object Oriented 
                 Programming with PHP5','author' => 'Hasin Hayder',
                'isbn' => '1847192564',
                'releaseDate' => 'December 2007'
            ),
            '1' => array (
                'bookTitle' => 'Building Websites 
                 with Joomla! v1.0',
                'author' => 'Hagen Graf',
                'isbn' => '1904811949',
                'releaseDate' => 'March 2006'
            )
        );
        $id =intval($id);
//      $id = $this->strip_and_clean( $id, $books);// the definition of this method is in AppController
        $id = $this->Util->strip_and_clean($id,$books);// the definition of this method is in UtilComponent

        $this->set($books[$id]);
        $this->set('pageHeading', 'Packt Book Store');
        $this->pageTitle = 'Welcome to the Packt Book Store!';
    }
}
?>