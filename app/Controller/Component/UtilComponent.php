<?php
App::uses('Component', 'Controller');

class UtilComponent extends Component {
       function strip_and_clean ( $id, $array) {
            $id = intval($id);
            if( $id < 0 || $id >= count($array) ) {
                $id = 0;
            }
            return $id;
        }
    }

?>